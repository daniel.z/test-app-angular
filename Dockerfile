# Stage 0, "build-stage", based on Node.js, to build and compile the frontend
FROM node:20 as build-stage
WORKDIR /app
COPY package*.json /app/
RUN npm install
COPY ./ /app/
ARG configuration=production
RUN npm run build -- --output-path=./dist/out --configuration $configuration

# Stage 1, based on Node.js, to serve the compiled app
FROM node:20
WORKDIR /app
COPY --from=build-stage /app/dist/out/browser/ /app
RUN npm install -g serve
CMD ["serve", "-s", ".", "-l", "3000"]
